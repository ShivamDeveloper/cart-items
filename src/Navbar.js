import React from 'react'
import Cart from "./Cart"
import App from "./App"
const Navbar =(props)=>{
        let countitems=0;
        props.products.map((prod)=>{
            countitems+=prod.qty;
        })
        return(
            <div className="navbar">
                <img className="addcart" src="https://www.flaticon.com/svg/static/icons/svg/3932/3932143.svg">
                </img>
                <span style={{margin:5}}>{countitems}</span>
                <img className="addwishlist" src="https://www.flaticon.com/premium-icon/icons/svg/2767/2767003.svg"></img>
                <span>2</span>
            </div>
        );
    
}
export default Navbar