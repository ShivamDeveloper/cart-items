import React from "react";
import Cartitem from "./cart-items"
import App from "./App"
const Cart=(props)=>{
    
        const {products}=props.products;
        console.log("props is:",products);
        return(
            <div className="cart">
                {
                    props.products.map((product)=>{
                        return  <Cartitem product={product} key={product.id} increaseqty={props.increaseqty} deleteproduct={props.deleteproduct} decreaseqty={props.decreaseqty} />
                    })
                }
               
            </div>
        );
    
  
};

export default Cart;