
import React from 'react';
import Cart from "./Cart";
import Navbar from "./Navbar"
class App extends React.Component {
  constructor(){
    super();
    this.state={
    products:[
        {
        price:999,
        title:"phone",
        qty:1,
        img:'',
        id:1
    },
    {
        price:999,
        title:"Watch",
        qty:1,
        img:'',
        id:2
    },
    {
        price:999,
        title:"Tablet",
        qty:1,
        img:'',
        id:3
    }]
 }
}
handleincreaseqty=(product)=>{
     console.log("Hey plz increase the qtyb of this product",product);
     const {products}=this.state;
     const index=products.indexOf(product);
     console.log("index is:",index);
     products[index].qty=products[index].qty+1;
     this.setState({
         products:products
     })
}
handledecreaseqty=(product)=>{
    console.log("Hey plz increase the qtyb of this product",product);
    const {products}=this.state;
    const index=products.indexOf(product);
    console.log("index is:",index);
    if(products[index].qty>0){
        products[index].qty=products[index].qty-1;
    }
    else{
        products[index].qty=products[index].qty
    }
  
    this.setState({
        products:products
    })
 }
 deleteproduct=(id)=>{
        var {products}=this.state;
        let items=products.filter((item)=>item.id!==id);
        this.setState({
            products:items
        })
    }
  render(){
    const {products}=this.state;
    return (
      <div className="App">
          <h1>Cart</h1> 
          <Navbar products={products} />
          <Cart products={products} increaseqty={this.handleincreaseqty} decreaseqty={this.handledecreaseqty} deleteproduct={this.deleteproduct} />
      </div>
    );
  }
 
}

export default App;
